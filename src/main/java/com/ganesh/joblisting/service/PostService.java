package com.ganesh.joblisting.service;

import com.ganesh.joblisting.model.Post;
import com.ganesh.joblisting.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostService {

    @Autowired
    PostRepository postRepository;

    public Post savePost(Post post) {
        return postRepository.save(post);
    }

    public List<Post> getPosts() {
        return postRepository.findAll();
    }

    public String deletePost(String id) {
        postRepository.deleteById(id);
        return "Post deleted..."+id;
    }

    public Post updatePost(Post post) {
        Post existingPost=postRepository.findById(post.getId()).orElse(null);
        existingPost.setProfile(post.getProfile());
        existingPost.setDesc(post.getDesc());
        existingPost.setExp(post.getExp());
        existingPost.setTechs(post.getTechs());
        return postRepository.save(existingPost);

        }

    }

