package com.ganesh.joblisting.controller;

import com.ganesh.joblisting.model.Post;
import com.ganesh.joblisting.service.PostService;
import org.apache.catalina.filters.ExpiresFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.util.List;

@RestController
public class PostController {

    @Autowired
    PostService postService;


    @ApiIgnore
    @RequestMapping(value="/")
    public void redirect(ExpiresFilter.XHttpServletResponse response) throws IOException {
        response.sendRedirect("/swagger-ui.html");
    }

    @PostMapping("/addPost")
    public Post addPost(@RequestBody Post post) {
        return postService.savePost(post);
    }

    @GetMapping("/allPosts")
    public List<Post> getAllPosts() {
        return  postService.getPosts();
    }


    @DeleteMapping("/delete/{id}")
    public String deletePost(@PathVariable String id) {
        return postService.deletePost(id);
    }

    @PutMapping("/post/{id}")
    public Post updatePost(@RequestBody Post post) {
       return  postService.updatePost(post);
    }

}
